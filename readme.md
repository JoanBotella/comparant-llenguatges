
# Comparant Llenguatges

Abans d'executar les comandes indicades, canvia't al subdirectori del llenguatge.

## C (llenguatge compilat)

1. Compilar

El programa té dos arxius de codi font: `main.c` i `greeter.c` . El `main.c` és el punt d'entrada al programa, i l'únic que fa es cridar a una funció que està definida al `greeter.c`. Quan els compilem, obtenim els arxius de codi objecte `main.o` i `greeter.o`, amb algo proper al codi màquina d'allò que es fa en cada arxiu `.c` . Aquestos arxius encara no són executables.

```
$ gcc -c main.c greeter.c
```

2. Enllaçar

El programa enllaçador uneix els arxius de codi objecte entre sí, i els ho afegeix les llibreries externes que foren necessàries. Així, l'arxiu `greeter.o` no inclou el codi necessari per mostrar coses per pantalla, solament una crida a la funció `printf`, que és la que sap fer-ho. L'enllaçador haurà d'afegir el codi de la llibreria `stdio.h`, que és on està definida aquesta funció. El procés d'enllaçat acaba generant l'arxiu binari executable del nostre programa.

```
$ gcc -o main main.o greeter.o
```

3. Executar

Com ja tenim el programa com un arxiu binari executable, podem executar-lo directament al nostre sistema operatiu. Aquest arxiu binari és específic per al nostre tipus de sistema operatiu i tipus de processador. Per exemple, un kernel Linux 5.15.0-84-generic sobre un processador x86_64.

```
$ chmod u+x main
$ ./main
```

## Python (llenguatge interpretat)

1. Executar

En el cas de Python, una vegada tenim el codi font escrit, ja solament tenim que passar-li'l al programa intèrpret que tenim instal·lat al nostre sistema per a que l'execute. L'intèrpret anirà llegint les instruccions de cada un dels arxius i executant-les.

```
$ python3 main.py
```

## Java (llenguatge híbrid compilat/interpretat)

1. Compilar

Quan compilem en Java, els arxius de codi objecte, amb extensió `.java`, s'anomenen bytecode. Aquests arxius no són executables, i de fet, el codi màquina que contenen no és específic d'un tipus de sistema operatiu i un tipus de processador, sinó que solament els podrà executar/interpretar una Màquina Virtual de Java, que és un programa.

```
$ javac demo/Main.java demo/Greeter.java
```

2. "Enllaçar"

No se sol dir que els programes en Java s'enllacen, però el recomanable és unir tots els arxius `.java` del nostre programa en un sol arxiu `.jar`. Això podria entendre's com a enllaçar els arxius de bytecode.

```
$ jar -cfe Main.jar demo.Main demo/Main.class demo/Greeter.class
```

3. Interpretar

Finalment, podem utilitzar la Màquina Virtual de Java per a executar/interpretar el bytecode que s'ha compilat expressament per a ella. Hi ha una Màquina Virtual de Java per a cada sistema operatiu, però el bytecode del nostre programa és igual per a totes elles.

```
$ java -jar Main.jar
```